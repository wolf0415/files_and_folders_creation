from fs.osfs import OSFS
from termcolor import colored
import sys

def main():
    courses = []
    rootFolder = input("Enter the name of the folder: ")
    print(colored('--------------------------------------------',color='yellow'))
    num = ""
    while not isinstance(num,int):
        try:
            num = int(input("Number of courses: "))
        except ValueError:
            print(colored("PLEASE ENTER A","red") + colored("NUMBER","red",attrs=["bold"]))

    for i in range(num):
        courses.append(input("Name of the courses: ").replace(" ", "_"))  # replace input space with underscore

    with OSFS(".") as currentDir:
        if(not currentDir.exists(f"{rootFolder}")):
            currentDir.makedir(f"{rootFolder}")
            print(colored("[+] Directory created", "blue",attrs=['bold']))

    for course in courses:  # loop through each course in courses' list
        with OSFS(f"{rootFolder}/") as newDir:
            if(not newDir.exists(course)):  # check if the folder with the name exists
                newDir.makedir(course)
                print(colored("[+] Folder created!!", "blue",attrs=['bold']))

            with OSFS(f"{rootFolder}/{course}/") as newDir:
                if(not newDir.exists("")):
                    newDir.makedir(course)
                    print(colored("[+] Folder created!!", "blue",attrs=['bold']))
                if(len(newDir.listdir(path=".")) == 0):  # check if the dir is empty
                    for i in range(1, 13):
                        a = f"week_{i}"
                        newDir.makedir(a)
                    print(colored("[*] week's folder created", "blue",attrs=['bold']))
                # list all the folder and files in the current directory
                weekList = newDir.listdir(".")
                try:
                    for i in weekList:  # loop through each of the folder that labeled with weeks
                        # Create these 3 because they are the most common folder to store files we need.
                        newDir.makedir(f"./{i}/Lecture")
                        newDir.makedir(f"./{i}/Practical")
                        newDir.makedir(f"./{i}/Workshop")
                        print(colored(f"    [+] Folder in {i} created successfull", "blue",attrs=['bold']))
                except Exception:
                    print(colored("[!] Folder already exists.", "red",attrs=['bold']))

    with OSFS(f"{rootFolder}/") as currentDir: 
        # list all the directory
        # same as "tree" command in linux
        currentDir.tree()

if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt: # in vs code, must CTRL+C and press ENTER to quit
        print("Program closed")
        sys.exit(0)